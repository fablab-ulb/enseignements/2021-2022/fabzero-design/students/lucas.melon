Hey,
voici le blog personnel de Lucas Melon pour Le fabZero

## Qui ?

![](images/avatar-photo.jpg)

Bonjour je m'appel Lucas je suis étudiant en architecture a la facultée LaCambre Horta a Bruxelles.
Ce site répertorie les travaux et expérimentations effectués pour l’option design prenant place au FabLab de l’ULB.

## Mon parcours

Je suis née à Braine-l’Alleud ou j’ai suivi un cursus scientifique avec en parallèle une formation pluridisciplinaire à l’école des arts de Braine-l’Alleud. Je suis ensuite passé à l’IPET de Nivelle pour finir mon cursus avec une option Science industrielle spécialisée dans la construction avant de commencer mes études d’architecture.

## Mes Travaux précédant

Je tente de diversifier mes créations dans plusieurs domaines artistiques. Outre l’architecture je peins beaucoup (toutes mes peintures sont visibles sur mon compte [Instagram](https://www.instagram.com/lucas_melon_/?hl=fr)) de plus ma formation a l’école des arts ma permit de tester beaucoup de technique (sculpture, gravure, céramique, etc.) que je tente de mêler dans mes projets.
![](images/tableau1.jpg)
![](images/tableau2.jpg)

### Projet A

Mon premier projet portera sur une table à dimension variable. Cette idée vient d’un besoin de ma collocation actuelle d’avoir une grande table pour inviter des gens qui ne soient pas encombrants au quotidien. Une première version a été créée pour ma collocation à partir de palette , je souhaite garde me mouvement, mais en optimisant la solidité et la facilité d’emplois tout en y mêlant une esthétique d’horlogerie avec des rouages et des mécanismes apparents.

![](images/table_f1.jpg)

![](images/table_o1.jpg)

![](images/table_f2.jpg)

![](images/table_o2.jpg)
