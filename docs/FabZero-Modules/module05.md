# 5. Usinage assisté par ordinateur

Ce module s’attarde sur le fonctionnement d’une CNC portable Shaper Origin

## Fonctionnement

La Shaper tool est une CNC guider à la main qui permet d’usiner des éléments de grande dimension directement sur un chantier.

![](../images/shaperd0.jpg)

Son utilisation se base sur un guide numérique, la machine créer une zone de travail grâce à des repères visuels (shaper tape) et calque un dessin sur cette surface. Ensuite l’écran permet de suivre le dessin avec une zone de tolérance dans laquelle la machine peut compenser un faux mouvement.

Les détails techniques et les bases de données pour les ajustements de vitesse et de profondeur sont disponibles via ce [Lien](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/Shaper.md).

## Mises en place

La première étape pour la découpe est la préparation de la zone, la surface à découper doit être fixe (plan de travail stable et planche collés à celui-ci) le Shaper tape dois être positionné horizontalement 15 cm au-dessus du point de découpe.

![](../images/shaperd1.jpg)

Une fois les tapes mises en place on peut scanner la zone de travail avec la machine. L’indicateur en forme de domino en haut à droite permet de vérifier que toute la zone de découpe a suffisamment de Shaper tape, ensuite si la zone est prête on peut importer un dessin en .svg

![](../images/shaperd2.jpg)

![](../images/shaperd3.jpg)

Pour finir la préparation, on place le dessin sur la zone de découpe et on revérifie que toute la zone est accessible de façon stable et couverte par les repères visuels.

![](../images/shaperd4.jpg)

## Découpe

Avant de découper on règle la vitesse de rotation en fonction du matérielle et la profondeur de découpe, la fraiseuse et le ventilateur doivent être allumés individuellement avant de commencer à usiner.

![](../images/shaperd5.jpg)

Pendant la découpe on déplace la machine le long du guide numérique, il faut avancer dans la direction donner par la machine et garder le guide à l’intérieur du cercle blanc.

![](../images/shaperd6.jpg)
