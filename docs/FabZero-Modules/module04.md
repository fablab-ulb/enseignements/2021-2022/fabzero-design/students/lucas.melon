# 4. Découpe assistée par ordinateur

Cette semaine je travaille sur un abat-jour pour une lampe de mon bureau.

## Experimentation

Le matériel utilisé est une planque de polypropylène translucide simulée ici par une feuille de papier épaisse.

La lampe sur pied qui me sert de base pour ce projet est une lampe unidirectionnelle qui forme un cercle lumineux très peu confortable et pratique pour travailler, mon premier but sera donc de tamiser cette lumière pour la rendre plus homogène. Le second but sera de disposer d’une pièce facilement attachable sur n’importe quelle lampe de ce modèle.
Avec ces informations en tête, je commence à faire des prototypes, les premiers se basent sur des crochets, la tension dans la feuille permettrait de maintenir l’abat-jour en place.

![](../images/lampe1.jpg)

J’effectue aussi des recherches sur la diffusion j’en arrive à la conclusion qu’il me faut pour une bonne diffusion soit 2 couches écarter de polypropylène ou une couche en oblique.

![](../images/lampe2.jpg)

## Modèle fonctionnel

Sur base de ces principes je développe 2 modèles l’un repose sur des filtres perpendiculaires à la lumière et des attaches en crochet l’autre repose sur une logique de panier qui encercle la lampe.

![](../images/lampe3.jpg)

![](../images/lampe5.jpg)

![](../images/lampe4.jpg)

## Conception numerique

Une fois mes 2 modèles passés sur Auto CAD je créer plusieurs claques pour mes découpes dans cet ordre :
-pour les gravures légères (uniquement esthétique aucun effet sur la lumière)
-pour les gravures profondes (corresponds au pli)
-pour les coupures intérieures
-pour les coupures extérieures

![](../images/Lampe1.svg)

On peut placer 6 exemplaires de mon premier modèle sur une surface de 44cmx66cm avec un pourcentage de perte de 33,2%

![](../images/lampe7.jpg)

![](../images/Lampe2.svg)

## Découpe

Pour configurer ma découpe j’attribue à chaque « passage » une couleur qui correspond à un claque, ensuite je classe mes calque dans le bonne ordre (je commence par les gravures et puis les découpe en commencent par l’intérieur) le but étant qu’aucune pièces de soit entièrement détaché de la feuille au moment de la découpe, pour finir j’attribue a chaque claque une valeur de vitesse et de puissance du laser a l’aide des tests de découpe et de plis réalisé précédemment.

Pour le calque « gravure légère » je choisis 800 mm/min pour 4% de puissance  
Pour le calque « gravure profonde » je choisis 800 mm/min pour 10% de puissance  
Pour le calque « découpe intérieur » je choisis 400 mm/min pour 50% de puissance  
Pour le calque « découpe extérieur » je choisis 600 mm/min pour 65% de puissance  

![](../images/lampe8.jpg)

Après avoir préparé le fichier .svg , je calibre la hauteur du laser grâce à une pièce en plastique prévu pour ça, et place ma feuille que je bloque avec des pièces en métal en dehors de la zone de découpe.
La fonction move du software permet de prévisualiser l’emplacement du laser pour le début de la découpe et donc d’économiser du matériel en placent la découpe dans un coin de la feuille.

![](../images/lampe9.jpg)

## Résultat

Modèle A

![](../images/lampe10.jpg)

![](../images/lampe11.jpg)

Modèle B

![](../images/lampe12.jpg)

![](../images/lampe13.jpg)

## Pied de Lampe

Si vous ne disposez pas de la lampe de base j’ai également créé une attache murale compatible avec mon 2eme modèle. A l’aide d’un socket E14 et d’une alimentation vous pouvez facilement fabriquer cette lampe murale.

![](../images/lampe15.svg)

L’attache murale est très simple et ne comporte qu’un seul pli. Le poids réduit de la lampe permet de la fixer au mur avec seulement 2 punaises.

![](../images/lampe14.jpg)

![](../images/lampe15.jpg)
