# 1. Documentation

L’objectif de cette semaine est la définition de mon sujet de travail et la prise en main des outils Git.

## logiciel
La première étape consiste à installer un logiciel de traitement de texte capable de prend en charge des fichiers .md (ici Atom disponible sur ce [lien](https://atom.io/) )
![](../images/1atom.jpg)

Ensuite pour les utilisateurs Windows installer Git pour Windows disponible a ce [lien](https://git-scm.com/) (installer aussi Git Bash via le même installateur)
![](../images/2gitwind.jpg)
![](../images/3gitwind.jpg)

## config de base

Lancer Git Bash et commencer par les configurations simples (pseudo, adresse mail)
![](../images/4gitconfig.jpg)

## clé SSH

Une fois les bases misent en place la première étape pour connecter votre ordinateur à Gitlab est de générer une clé SSH (ici le forma ed25519 mais d’autres forma existent).
![](../images/5keyssh.jpg)

Ensuite, copier cette clé dans votre presse papier grâce à la commande spécifique à Windows et la coller dans les paramètres de compte GitLab dans la section SSH.

## clonage

Ensuite vous pouvez cloner votre projet sur votre PC en utilisant le bouton Copy sur le site GitLab, sélectionner la solution SSH et taper la dans votre Git Bash précédé de git clone (il est important d’ouvrir Git Bash dans le dossier de destination)
![](../images/6clone.jpg)

## remote et pull

Pour finir on créer un dossier .git avec git init et on créer une origine grâce à git remote add origin (placer l’url de la page entre git@ et .git avec « : » entre .com et Fablab
![](../images/7configpull.jpg)

Le pull est maintenant possible avec git pull origin main (pour la main branch)
![](../images/8pull.jpg)

## status

Via les 3 commandes checkout, diff et status on peut obtenir des informations sur les changements à effectuer.
![](../images/9status.jpg)

## commit

Pour faire un push il faut d’abord charger un commit grâce a la commande git add (dossier ou fichier).
![](../images/10comit.jpg)

## push

Pour finir la commande push envoie les données vers Gitlab.
![](../images/11push.jpg)
