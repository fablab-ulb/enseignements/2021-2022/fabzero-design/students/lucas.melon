# 2. Conception Assistée par Ordinateur

Cette semaine je modélise ma base de projet sur fusion 360.

## Modelisation du Pied

Pour la modélisation de ma table, j’ai commencé par créer un sketch qui reprend les dimensions d’une europalette. Le pied étant composé de 3 palettes ce sketch va me servir pour monter le pied.
![](../images/fusionsketch1.jpg)

Je commence donc par extruder les premiers éléments de mon pied.
![](../images/fusionextru1.jpg)

Pour la seconde extrusion, je choisis dans « démarrer » l’option « décalage » que je fais correspondre à la hauteur de mon premier élément. Avec cette technique je vais pouvoir utiliser 1 seul sketch pour mon pied.
![](../images/fusionextru2.jpg)

Arriver à la seconde palette je créer un nouveau corp afin de bien différentier mes éléments. De plus je choisis dans « démarrer » l’option « objet » qui me permet de directement préciser la hauteur du début de l’extrusion, cette solution est plus facile.
![](../images/fusionextru3.jpg)

Je procède de la même façon pour monter le reste du pied.
![](../images/fusionextru4.jpg)

Toutes les planches ont été conservées sur le dessus de la dernière palette, je dessine donc un sketch pour ces planches. Je joins ces planches au corps de la dernière palette.
![](../images/fusionsketch2.jpg)

![](../images/fusionextru5.jpg)

Pour la surface de la table, je créer un sketch avec les 3 parties et je les extrude séparément pour obtenir 3 éléments indépendants.
J’ajoute également une variable sur la largeur de la table qui peut être changer facilement sans impacter le fonctionnement de la table.
![](../images/fusionextru6.jpg)

## Modelisation du mécanisme

Ma table est presque terminée il me reste à créer le mécanisme de maintien. Je créer un sketch qui reprend le bras mobile, son soutient et la cale de fin.
![](../images/fusionsketch3.jpg)

Je commence par extruder la cale en 2 parties que je joins à ma surface de table.
![](../images/fusionextru7.jpg)

Ensuite je fais un réseau circulaire autour de l’axe bleu avec mes 2 extrusions de sorte à avoir une cale de chaque côté.
![](../images/fusionrotation1.jpg)

Je monte les 2 soutiens des bras mobiles et je les joins à ma palette supérieure.
![](../images/fusionextru8.jpg)

Pour les bras mobiles, je crée un nouveau corp sur lesquelles je fais le même réseau circulaire que pour les cales. Un trou situé à la base du bras me servira plus tard comme axe de rotation.
![](../images/fusionextru9.jpg)

## texture

Je peux maintenant ajouter des textures pour rendre mon projet plus lisible dans l’onglet rendu.
![](../images/fusiontexture.jpg)

Pour avoir un rendu de la table fermer je n’ai qu’à faire une rotation des bras et des côtés de la table.
![](../images/fusionv2rotation1.jpg)

![](../images/fusionv2rotation2.jpg)

![](../images/fusionv2texture.jpg)


## modèle 3D

[Fichier Fusion360](https://a360.co/3nMpAtf)
[Fichier .obj](../images/fichiers/Tablefermer.obj)
[Fichier STL](../images/fichiers/tablemecanique.stl)

<div class="sketchfab-embed-wrapper"> <iframe title="Table mecanique fermé" frameborder="0" allowfullscreen mozallowfullscreen="true" webkitallowfullscreen="true" allow="autoplay; fullscreen; xr-spatial-tracking" xr-spatial-tracking execution-while-out-of-viewport execution-while-not-rendered web-share width="640" height="480" src="https://sketchfab.com/models/a413c234cc59428f9904cc5e2fe598ac/embed"> </iframe> <p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;"> <a href="https://sketchfab.com/3d-models/table-mecanique-ferme-a413c234cc59428f9904cc5e2fe598ac?utm_medium=embed&utm_campaign=share-popup&utm_content=a413c234cc59428f9904cc5e2fe598ac" target="_blank" style="font-weight: bold; color: #1CAAD9;"> Table mecanique fermé </a> by <a href="https://sketchfab.com/Lucas.melon?utm_medium=embed&utm_campaign=share-popup&utm_content=a413c234cc59428f9904cc5e2fe598ac" target="_blank" style="font-weight: bold; color: #1CAAD9;"> Lucas.melon </a> on <a href="https://sketchfab.com?utm_medium=embed&utm_campaign=share-popup&utm_content=a413c234cc59428f9904cc5e2fe598ac" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a></p></div>

<div class="sketchfab-embed-wrapper"> <iframe title="Table mecanique ouverte" frameborder="0" allowfullscreen mozallowfullscreen="true" webkitallowfullscreen="true" allow="autoplay; fullscreen; xr-spatial-tracking" xr-spatial-tracking execution-while-out-of-viewport execution-while-not-rendered web-share width="640" height="480" src="https://sketchfab.com/models/636da9b46b2441c9bfb134e9b4bb0b07/embed"> </iframe> <p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;"> <a href="https://sketchfab.com/3d-models/table-mecanique-ouverte-636da9b46b2441c9bfb134e9b4bb0b07?utm_medium=embed&utm_campaign=share-popup&utm_content=636da9b46b2441c9bfb134e9b4bb0b07" target="_blank" style="font-weight: bold; color: #1CAAD9;"> Table mecanique ouverte </a> by <a href="https://sketchfab.com/Lucas.melon?utm_medium=embed&utm_campaign=share-popup&utm_content=636da9b46b2441c9bfb134e9b4bb0b07" target="_blank" style="font-weight: bold; color: #1CAAD9;"> Lucas.melon </a> on <a href="https://sketchfab.com?utm_medium=embed&utm_campaign=share-popup&utm_content=636da9b46b2441c9bfb134e9b4bb0b07" target="_blank" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a></p></div>

![](../images/fusionrendu.jpg)
