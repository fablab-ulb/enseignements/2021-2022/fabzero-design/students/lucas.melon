# 3. Impression 3D

Pour cette impression j’ai créé un modèle de pince pour maintenir une nappe de protection sur une table à l’aide de fusion 360. Le modèle est assez simple, mais comporte de petites extrusions avec un profile de dent.
Ce modèle est l’amélioration d’un premier modèle qui prévu pour plusieurs épaisseurs de table et calqué sur un modèle en métal c’est montré très peu efficace.

V1
![](../images/impression0.1.jpg)

V2
![](../images/impression0.jpg)

[Fichier Fusion360](https://a360.co/3bKQPyZ)
[Fichier STL](../images/fichiers/attachenappev2.stl)

## Prusa Slicer

J’imprime habituellement avec Simplifie3D je découvre donc Prusa Slicer, je commence par vérifier les paramètres de base je veux une impression rapide, mais solide, j’opte donc pour un remplissage de 15% et une épaisseur de couche de 0.2mm.

![](../images/impression1.jpg)

Je n’ai jamais eu de problème de décollement avec le bed du Ender 3 pro je me passe donc de raft pour cette impression, je garde juste une jupe de 2 tours décollée du modèle pour m’assurer que du plastique soit dans la buse au début de l’impression.

![](../images/impression2.jpg)

Pour finir, je m’assure que l’angle des extrusions ne nécessite pas de support (je n’ai pas constaté de problème en dessous de 70° donc je me passe de support).

![](../images/impression3.jpg)

## L'imprimante

Une fois le Gcode exporté vers une carte SD je peux commencer à préparer mon imprimante.

![](../images/impression4.jpg)

La première étape après le nettoyage du plastique de l’ancienne impression est le nivelage du plateau à l’aide des 4 molettes situées sous le plateau (pas nécessaire sur les Prusa du FabLab). Pour niveler le plateau on applique la fonction « auto-home » basiquement on met l’axe Z a 0.0, ensuite on bouge la buse au niveau d’une molette et on passe une feuille de papier entre le plateau et la buse (qui doit être froide) il faut ajuster la molette pour que l’on puisse bouger la feuille, mais avec une certaine résistance et répéter le processus pour chaque molette (plusieurs passages sont parfois nécessaires).

![](../images/impression5.jpg)

Je vérifie aussi que les courroies soient bien tendues et je les réajuste si besoin.

![](../images/impression6.jpg)

Plus qu’à lancer l’impression via « print from SD » et à attendre.
Durant l’impression je dois juste faire attention a ce que les conditions de la pièce ne change pas trop au cours de l’impression (une trop grande différence de température ou d’humidité se verra sur l’impression).

## Resultat

![](../images/impression7.jpg)

![](../images/impression8.jpg)
