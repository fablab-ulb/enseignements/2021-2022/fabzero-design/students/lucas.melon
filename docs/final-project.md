# Final Project

## 1 Déconstruction
Déconstruire la fonction de l'objet
  neutre et détaché, précision
  point de vue divers (colorer le regard)

  table: surface horizontale
  support , surface disponible poser des choses
  éléments physiques , déplaçables
  continuité de surface
  résistance physique

## 2 Situation
résoudre un vrai problème
 contrainte et limite (intrinsèque)
 définition de l'objet
 se poser des règles
 trouver des défauts(problème à corriger)

 3 Situations:

 Repas (peu)
  genou sous la table
  3 assiettes et un plat (poids)

 Soir(peu)
  circulation autour de la table
  4-5 places
  repose-pied ?

 Soir (beaucoup)
  circulation autour de la table
  6-7 places

 Problème : place limitée, multifonctionnalité

## 3 Dimensions
Dimension spécifique
 ergonome => poids , geste , dimensionnement du corp
 strate de règle de dimension.

surface: je prend pour base les dimensions d'un set de table.
![](../images/plat1.jpg)

Repas (peu):
![](../images/repas1.jpg)
![](../images/repas2.jpg)

Soir(peu):
![](../images/soir1.jpg)
![](../images/soir2.jpg)

Soir (beaucoup):
![](../images/soir3.jpg)

## 4 Esquisse
esquisse, amélioration
 proto sale, proto propre , proto fonctionnels
 intégration des technologies

### 4.1 Algodoo

test sur Algodoo.
 L’objectif est de créer 2 « ailettes » surélevées de 20 cm dont le déploiement ne gênerait pas les jambes.
 Si possible je cherche à pouvoir changer la hauteur de la table indépendamment des ailettes.

Pour le développement de mon mécanisme je dresse une liste d’exigence pour classer et évaluer la pertinence d’un mécanisme.

| Mouvement                                                          | Proto 1 | Proto 2 | Proto 3 | Proto 4 | Proto 5 | Proto 6 | Proto 7 | Proto 8 | Proto 9 | Proto 10 |
|--------------------------------------------------------------------|---------|---------|---------|---------|---------|---------|---------|---------|---------|----------|
| Fluide                                                             |  ok     |  ok     |  ok     |  ok     |  non    |  non    |  ok     |  ok     |  ok     |  ok      |
| Repli total de l’ailette                                           |  non    |  non    |  non    |  ok     |  non    |  non    |  non    |  ok     |  ok     |  ok      |
| Pas de contact avec le genou ou le canapé pendant le déploiement   |  ok     |  ok     |  ok     |  ok     |  ok     |  ok     |  ok     |  ok     |  ok     |  ok      |

| Mecanique                                                          | Proto 1 | Proto 2 | Proto 3 | Proto 4 | Proto 5 | Proto 6 | Proto 7 | Proto 8 | Proto 9 | Proto 10 |
|--------------------------------------------------------------------|---------|---------|---------|---------|---------|---------|---------|---------|---------|----------|
| Minimiser les chaines de rouage                                    |  0      |  1      |  1      |  1      |  2      |   2     |   1     |  3      |  2      |  2       |
| Minimiser les axes                                                 |  0      |  2      |  2      |  2      |  4      |   4     |   2     |  6      |  3      |  2       |
| Minimiser les pièces mobiles                                       |  5      |  12     |  10     |  12     |  8      |   12    |   12    |  14     |  7      |  10      |
| Principe non dépendant de dimension précise                        |  ok     |  non    |  non    |  non    |  ok     |   ok    |   non   |  non    |  ok     |  ok      |

| Optionnel                                                          | Proto 1 | Proto 2 | Proto 3 | Proto 4 | Proto 5 | Proto 6 | Proto 7 | Proto 8 | Proto 9 | Proto 10 |
|--------------------------------------------------------------------|---------|---------|---------|---------|---------|---------|---------|---------|---------|----------|
| Mouvement indépendant des 2 ailettes                               |  ok     |  ok     |  ok     |  ok     |  ok     |  ok     |  ok     |  ok     |  non    |   non    |
| Hauteur de la partie centrale de la table indépendante             |  non    |  ok     |  ok     |  ok     |  ok     |  non    |  ok     |  ok     |  non    |   non    |

Proto 1

Le mouvement est correct, mais ne permet pas de modification de la hauteur et demande des cales et point de rotation située en dehors de la table.
![](../images/test1.1.jpg)
![](../images/test1.2.jpg)
<iframe width="560" height="315" src="https://www.youtube.com/embed/5pRtuE4Vj8w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Proto 2

Premier test basé sur 2 pistons, le rapport entre le mouvement des 2 pistons doit être très précis et permet difficilement une rétractation totale de l’ailette.
![](../images/test2.1.jpg)
![](../images/test2.2.jpg)
<iframe width="560" height="315" src="https://www.youtube.com/embed/p5fuzHv0Duc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Proto 3

Remplacement d’un piston par une crémaillère, le mécanisme n’est pas fonctionnel
![](../images/test3.1.jpg)
![](../images/test3.3.jpg)
<iframe width="560" height="315" src="https://www.youtube.com/embed/pBgdNEOfN2E" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Proto 4

Ajout d’un rail pour l’ailette que qui permet un retrait total à condition d’avoir une fixation en 2 parties à gauche de l’ailette.
![](../images/test4.1.jpg)
![](../images/test4.3.jpg)
<iframe width="560" height="315" src="https://www.youtube.com/embed/Kx-VeEtBeJ4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Proto 5

Test d’un mouvement en 2 étapes avec 2 axes, le mécanisme est plutôt stable, mais demande un rouage en dehors de la table et la transmission du mouvement entre les 2 axes peut être une grande source de friction.
![](../images/test5.1.jpg)
![](../images/test5.3.jpg)
<iframe width="560" height="315" src="https://www.youtube.com/embed/Bvbzif6XTEc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Proto 6

Version différente du même principe elle présente les mêmes défauts.
![](../images/test6.1.jpg)
![](../images/test6.3.jpg)
<iframe width="560" height="315" src="https://www.youtube.com/embed/Y_Bcf-vWfgk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Proto 7

Retour à un système de piston avec une charnière à glissière qui permet une plus grande liberté de mouvement.
![](../images/test7.1.jpg)
![](../images/test7.2.jpg)
<iframe width="560" height="315" src="https://www.youtube.com/embed/oApv7C23WM4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Proto 8

Combinaisons de la charnière à glissière avec un axe supplémentaire, le mouvement est élégant, mais demande bien trop d’éléments et donc bien trop de frottement.
![](../images/test8.1.jpg)
![](../images/test8.3.jpg)
<iframe width="560" height="315" src="https://www.youtube.com/embed/l2xFZiJJdt8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![](../images/test8.4.jpg)
![](../images/test8.5.jpg)
<iframe width="560" height="315" src="https://www.youtube.com/embed/TFe9MUV8Jmk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Proto 9

Le dernier système est très simple, ce qui est un avantage, mais le rail étant attaché à la surface de la table la hauteur des ailettes est lié à celle de la table.
![](../images/test9.1.jpg)
![](../images/test9.3.jpg)
<iframe width="560" height="315" src="https://www.youtube.com/embed/fzCONBQMRYE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Proto 10
![](../images/test10.1.jpg)
![](../images/test10.2.jpg)
![](../images/test10.3.jpg)
![](../images/test10.4.jpg)
<iframe width="560" height="315" src="https://www.youtube.com/embed/B3G4UJBhnGI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[Save Algodoo](../images/fichiers/tablemecanique.phz)

### 4.2 Cad

Une fois le principe de mon mécanisme mis au point je passe sur auto cad pour l’adapter aux dimensions précises de mon projet.

![](../images/protocad1.jpg)
Une rapide esquisse me permet de déterminer la taille maximale de mes engrenages pour qu’il n’entre pas en contact avec les ailettes durant le repli.

![](../images/protocad2.jpg)
Pour mes engrenages je dois juste calculer le rapport entre la distance parcourue par mes 2 glissières, celles des ailettes (37,95cm) et celles de la partie centrale (11cm), je choisis donc d’utiliser un engrenage de 19 dents et un autre de 6 dents (en partant du principe que chaque dent fait 2cm).

![](../images/protocad3.jpg)
Pour la plaque de connexion des engrenages, j’ai exploré plusieurs designs avec des formes assez complexes pour revenir à une forme unie avec un trou pour voir les engrenages qui sont suffisamment complexes en eux même.

![](../images/protocadr2.jpg)
![](../images/protocadr1.jpg)

Mon premier prototype sera découpé au laser à l’échelle 0.3 sur une plaque de hardboard de 3mm.
![](../images/decoupetablex2.svg)
![](../images/decoupetablex1.svg)

### 4.3 Test physique

Avant de passer au montage, j’ai quelque opération à effectuer :

Les engrenages une fois nettoyés doivent être collés pour doubler leurs épaisseurs, une cale supplémentaire vient se placer sur un côté.
![](../images/montage1.jpg)

Ensuite j’ai foré les trous pour les axes dans les engrenages et les plaques de connexion.
![](../images/montage2.jpg) ![](../images/montage3.jpg)

 Le montage commence par la mise en place des glissières sur les plaques, elles sont en 4 parties et permette de maintenir la barre de support de l’ailette, cette opération doit être répétée 4 fois (une pour chaque plaque).
 ![](../images/montage4.jpg)
 ![](../images/montage5.jpg)

Je dois également confectionner les charnières que je viendrais coller aux ailettes à la fin.
![](../images/montage6.jpg)

Pour finir l’assemblage de la partie basse, je dois placer les plaques et les engrenages dans l’ordre le long des axes et coller l’engrenage a l’axe. Les 4 petits engrenages sont collés au bout de l’axe (ils doivent être tous les 4 dans la même position).

Pour réduire les frictions des glissières, j’utilise du graphite micronisé qui est un lubrifiant sec qui ne sera pas absorbé par le bois.
![](../images/montage8.jpg)

Mon prototype révélé 2 erreurs critiques et un besoin de réduire les frictions.
![](../images/montage7.jpg)

La fin du rail supérieur ne fonctionne pas.
Solution : repenser le système avec une pente plus douce et une nouvelle charnière.

Les barres de maintien de la partie centrale sorte de la glissière
Solution : largeur incorrecte ou besoin d’une cale

![](../images/montage9.jpg)

### 4.4 Correction

Conception d’une nouvelle charnière, l’épaisseur plus large de la planche de surface permet de supprimer « l’encoche » présente sur le précédent modèle, la pente est également plus douce et la bordure du rail plus épais pour plus de solidité.

![](../images/conception0.jpg)

Ajout d’un rail de verrouillage sur les glissières verticales, l’utilisation de la CNC permet un travail des pièces en 3 dimensions.

![](../images/conception1.jpg)

Ensuite je redésigne toutes les pièces en privilégiant la rigidité du cadre qui accueille les parties mobiles et minimise les surfaces de frottement entre les cardes et celle-ci. Je rajoute un contreventement entre les 2 cadres et des renforts sous les surfaces de table pour éviter une flexion des planches.

![](../images/conception.jpg)

### 4.5 Modélisation et animation

Une fois mes correctifs appliqués, j’ai modélisé et animé tout mon projet sur Fusion 360 pour vérifier l’imbrication de toutes les pièces, cela m’a permis de corriger plusieurs pièces ( par exemple la pièce i1 était trop large et les rails des pièces M étaient 1 cm trop long ) ou de bien placer la manivelle pour qu’elle ne dépasse pas quand la table est rétractée.

![](../images/tablerochet1.jpg)

![](../images/tablerochet2.jpg)

<iframe width="560" height="315" src="https://www.youtube.com/embed/T4CQS1oiSpg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[Fichier Fusion360](https://a360.co/3FSky5l)

## 5 Diffusion
adaptation, diffusion
 reproductibilité , déclinaison => expression simple , offre des possibilités de série
 documentation

### 5.1 Usinage et préparation des pièces

Toutes les pièces modélisées en été découper à la CNC en suivant les planches ci-dessous, chaque pièce porte un nom utiliser pour l’assemblage.

![](../images/Plaque1.jpg)
![](../images/Plaque2.jpg)
![](../images/Plaque3.jpg)
![](../images/Plaque4.jpg)
![](../images/Plaque5.jpg)
![](../images/Plaque6.jpg)

Les pièces pouvent être reproduite en extrudant les DXF des planches de la hauteur précisées ( de 5.5mm pour le rail des pièces H et M )

[Plaque1](../images/fichiers/1ep1.2.dxf)
[Plaque2](../images/fichiers/2ep1.2.dxf)
[Plaque3](../images/fichiers/3ep1.2.dxf)
[Plaque4](../images/fichiers/4ep1.2.dxf)
[Plaque5](../images/fichiers/5ep1.8.dxf)
[Plaque6](../images/fichiers/6ep1.8.dxf)

Une fois les pièces découpées (ici avec une fraise de 8mm) certaines doivent être modifiées :

Le profil de la partie plus fine des pièces R doit être taillé de façons circulaires.
![](../images/5.1.1.jpg)

Le profil de la pièce U1 doit être taillé de biseau en suivant les indications suivantes.
![](../images/5.1.2.jpg)

Un couvre chant de 0.3mm doit être appliqué sur le bas des pièces K, L et le haut des pièces B pour réduire le frottement entre ces pièces.
![](../images/5.1.3.jpg)

Les axes créer à partir de tringle a rideaux en inox doivent être découpé a 90cm de long et remplis avec une pièce de bois sur les 15 derniers cm pour éviter les déformations, une des pièces de bois doit dépasser de 3cm du tube en métal.
![](../images/5.1.4.jpg)

![](../images/5.1.5.jpg)

### 5.2 Instruction de montage

Le montage commence par la construction de la base qui est constituée de 2 cadres reliés par les pièces J.

![](../images/montagebase.jpg)

Ensuite les pièces K et L doivent être insérées dans les cadres et les axes doivent être montés en passant par les cadres, les pièces N seront ajoutées à la fin du montage.

![](../images/montageaxe.jpg)

La surface de la table peut alors être montée, les panneaux latéraux composer des pièces T et R doivent être inséré dans les rails des pièces i une fois la surface mise en place sur la base.

![](../images/montagesurface.jpg)

La fin du montage consiste a attacher via les charnières ( pièces Q ) l’extrémité des pièces K et L a l’extrémité des panneaux latéraux et de fixer les pièces N sur les axes.

### 5.3 Déclinaison

Ce mécanisme d’extension peut être adapté de différente manière : s’il est retourné, on peut imaginer une table qui déploie des bancs sur les côtés, on peut également créer un volet réglable pour un puits de lumière ou une étagère modulable.

## 6 Nom
nom, pictogramme
 nom signifiant (induis une histoire)
 logo type clarifier l'esprit de l'objet

Pour le nom de ma table j’ai choisi « Table rochet », rochet fait référence à une pièce d’horlogerie que l’on retrouve dans dans les comtoises, un modèle de pendule en bois très répendu, ce nom fait donc référence à la fois au monde de l’horlogerie qui inspire cette conception mais également au bois des vieux pendules.

![](../images/logo.jpg)

Pièces de pendule appelé rochet.
![](../images/rochet.jpg)
